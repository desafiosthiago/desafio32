## Avaliação Automação de Testes

Olá, seja bem vindo a nossa avaliação para analistas de testes com foco em automação.

Nossos testes tem como objetivo avaliar diversos conhecimentos técnicos ligados a automação de teste e lógica de programação, nenhum dos testes é obrigatório, sinta-se confortável para solucionar aqueles que se adequam ao seu conhecimento, ao final de cada teste, disponibilizar a solução adotada em um repositório publico ou privado ([GitHub](https://github.com), [BitBucket](https://bitbucket.org), [GitLab](https://gitlab.com), caso opte por um repositório privado, fornecer acesso para este e-mail **danilo.zagato@cognizant.com**

Realizar as soluções utilizando **_Java_** como linguagem de programação, caso use alguma biblioteca da comunidade, utilizar o [Maven](http://rest-assured.io) para gerenciar as dependências.

Caso tenha alguma duvida, sugestão, crítica, elogio, ou simplesmente bater um papo, entre em contato: **danilo.zagato@cognizant.com**

`Desafio Selenium Web`

- 1 - Realizar uma pesquisa no site da OLX e imprimir SOMENTE titulo e valor dos CINCO primeiros resultados.

- 2 - Realizar uma pesquisa no site da OLX que contenha paginação, tirar um print do PRIMEIRO anuncio da segunda pagina.

(Utilizar Page Objects será um diferencial)

`Desafio Selenium Mobile com Cucumber (Consulta Placa Veiculo DETRAN)`
Obs. o APK encontra-se neste mesmo repositório 

- 1 - Consultar uma placa valida e validar o teste com TestNG ou JUnit

- 2 - Consultar uma placa invalida e validar o teste com TestNG ou JUnit

(Bonus - Armazenar as placas validas em uma lista para um possível relatório)

`Desafio Api Rest`

- 1 - Este Endpoint http://services.groupkt.com/country/get/all retorna todos os países, realizar dois testes, validar a quantidade de países e verificar se um pais informado pelo usuário está presente (Nome, alpha2_code ou aplha3_code).

- 2 - Este endpoint http://services.groupkt.com/state/get/{countryCode}/all retorna dados de países (código) que o usuário informou, realizar uma busca em um determinado país informado pelo usuario e realizar uma busca textual em um determinado pais.
Guia de utilização do endpoint (http://www.groupkt.com/post/5c85b92f/restful-rest-webservice-to-get-and-search-states-and-territories-of-a-country.htm)

(Indicamos utilizar [Rest Assured](http://rest-assured.io). por ser fácil e ter bastante material na comunidade, entretanto, fique a vontade para utilizar o framework que achar melhor)

`Desafios Casos de Teste`

- 1 - Escolher um item do desafio Selenium, um do Selenium Mobile e um desafio Api Rest, escrever um caso de teste para cada. Escolher o formato que mais lhe agradar, ambos devem estar em Doc ou PDF e podem estar todos em um único arquivo devidamente separado.

**BOA SORTE**
